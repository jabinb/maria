﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using RoslynPad.Editor;
using RoslynPad.Roslyn;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Microsoft.CodeAnalysis.Scripting;
using Microsoft.CodeAnalysis.CSharp.Scripting.Hosting;
using Microsoft.CodeAnalysis.Scripting.Hosting;

namespace MariaGUI.Scripting
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Editor : Window
    {
        private RoslynHost _host;
        private DocumentViewModel _currentViewModel;

        public Editor()
        {
            InitializeComponent();

            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            Loaded -= OnLoaded;

            _host = new RoslynHost(additionalAssemblies: new[]
            {
                Assembly.Load("RoslynPad.Roslyn.Windows"),
                Assembly.Load("RoslynPad.Editor.Windows")
            }, references: null);

            _currentViewModel = new DocumentViewModel(_host, null);

            editor.Focus();
            var workingDirectory = Directory.GetCurrentDirectory();
            var documentId = editor.Initialize(_host, new ClassificationHighlightColors(),
            workingDirectory, string.Empty);

            _currentViewModel.Initialize(documentId);
        }


        private void OnItemLoaded(object sender, EventArgs e)
        {
            var editor = (RoslynCodeEditor)sender;
            editor.Loaded -= OnItemLoaded;
            editor.Focus();

            var viewModel = (DocumentViewModel)editor.DataContext;
            var workingDirectory = Directory.GetCurrentDirectory();

            var previous = viewModel.LastGoodPrevious;
            if (previous != null)
            {
                editor.CreatingDocument += (o, args) =>
                {
                    var docArgs = new DocumentCreationArgs(args.TextContainer, workingDirectory, args.ProcessDiagnostics, args.TextContainer.UpdateText);
                    args.DocumentId = _host.AddRelatedDocument(previous.Id, docArgs);
                };
            }

            var documentId = editor.Initialize(_host, new ClassificationHighlightColors(),
                workingDirectory, string.Empty);

            viewModel.Initialize(documentId);
        }

        private void OnEditorKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var editor = (RoslynCodeEditor)sender;
                if (editor.IsCompletionWindowOpen)
                {
                    return;
                }

                var viewModel = (DocumentViewModel)editor.DataContext;
                //e.Handled = true;
            }
        }

        private void btnExecute_Click(object sender, RoutedEventArgs e)
        {
            _currentViewModel.Evaluate(editor.Text);
            //ExecuteScript(editor, _currentViewModel);
        }

        private async void ExecuteScript(RoslynCodeEditor editor, DocumentViewModel viewModel)
        {
            if (viewModel.IsReadOnly) return;

            viewModel.Text = editor.Text;
            await viewModel.TrySubmit();
        }

        
    }
}
