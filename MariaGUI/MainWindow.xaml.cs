﻿using Lidgren.Network;
using MahApps.Metro.Controls;
using Maria.IPC;
using Maria.IPC.Client;
using Maria.IPC.Messages;
using MariaGUI.Macro;
using MariaGUI.Scripting;
using MInject;
using Mono.CSharp;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MariaGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private static MainWindow _Instance;
        private static ServiceClient _client;
        private MessageProcessor _messageProcessor;

        public static ConcurrentDictionary<Guid, IMessage> receivedMessages = new ConcurrentDictionary<Guid, IMessage>();

        public MainWindow()
        {
            _Instance = this;
            InitializeComponent();

            _client = new ServiceClient();
            _client.RegisterReceivedCallback(this.OnReceivedMessage);
            _messageProcessor = new MessageProcessor();

        }

        private const string ARIA_PROCESS_NAME = "LegendsOfAria";
        private const string ARIA_EXE_PATH = @"F:\Games\Legends of Aria Beta\LegendsOfAria.exe";
        private const string ARIA_LOG_PATH = @"F:\Games\Legends of Aria Beta\LegendsOfAria_Data\output_log.txt";

        private int _ariaProcessId = -1;

        private void OnReceivedMessage(IMessage message)
        {


            if(message is ErrorResponse)
            {
                var error = (ErrorResponse)message;
                Console.WriteLine("[Maria] Received Error message " + error.ExceptionMessage);
            }
            else
            {
                //Console.WriteLine("[Maria] Received message type " + message.GetType().FullName);
            }
        }

        private async Task WatchAriaLog(Process p)
        {
            btnStartAria.Invoke(() => { txtAriaLog.Text = String.Empty; btnStartAria.IsEnabled = false; });

            //Wait for log to be created
            await Task.Delay(5000);

            foreach (string line in TailFrom(ARIA_LOG_PATH))
            {
                if (p.HasExited) break;

                if (line == null)
                {
                    await Task.Delay(100);
                    continue;
                }

                txtAriaLog.Invoke(() =>
                {
                    txtAriaLog.AppendText(line + "\n");
                    txtAriaLog.ScrollToEnd();
                });

                await Task.Yield();
            }

            btnStartAria.Invoke(() => btnStartAria.IsEnabled = true);
        }

        private Process getAriaProcessOrStart(string ariaPath)
        {
            Process p = Process.GetProcessesByName(ARIA_PROCESS_NAME).FirstOrDefault<Process>();

            if(p == null)
            {
                ProcessStartInfo startInfo = new ProcessStartInfo(ariaPath);
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                p = Process.Start(startInfo);
                Task OutputWatcher = WatchAriaLog(p);
            }

            return p;
        }

        private bool Inject()
        {
            Process p = getAriaProcessOrStart(ARIA_EXE_PATH);

            if (p == null)
            {
                return false;
            }

            //Check if LoA is running && whether we've already injected the process
            if (p.Id == this._ariaProcessId)
            {
                return true;
            }

            MonoProcess.Attach(p, out MonoProcess process);

            string mariaInjectPath = Assembly.GetAssembly(typeof(Maria.Inject.EntryPoint)).Location;
            
            byte[] assemblyBytes = File.ReadAllBytes(mariaInjectPath);
            IntPtr monoDomain = process.GetRootDomain();
            process.ThreadAttach(monoDomain);
            //process.SecuritySetMode(0);
            //process.DisableAssemblyLoadCallback();

            IntPtr mariaInjectImage = LoadAssemblyIntoTarget(process, Assembly.GetAssembly(typeof(Maria.Inject.EntryPoint)).Location);

            IntPtr classPointer = process.ClassFromName(mariaInjectImage, "Maria.Inject", "EntryPoint");
            IntPtr methodPointer = process.ClassGetMethodFromName(classPointer, "Main");

            process.RuntimeInvoke(methodPointer);
            //process.EnableAssemblyLoadCallback();
            process.Dispose();

            this._ariaProcessId = p.Id;

            return true;
        }

        private IntPtr LoadAssemblyIntoTarget(MonoProcess process, string path)
        {

            byte[] assemblyBytes = File.ReadAllBytes(path);
            IntPtr rawAssemblyImage = process.ImageOpenFromDataFull(assemblyBytes);
            IntPtr assemblyPointer = process.AssemblyLoadFromFull(rawAssemblyImage);
            IntPtr assemblyImage = process.AssemblyGetImage(assemblyPointer);

            return assemblyImage;
        }

        private void btnInject_Click(object sender, RoutedEventArgs e)
        {
            if (!this.Inject())
            {
                Console.WriteLine("Failed to Inject");
            }
        }

        private void btnStartAria_Click(object sender, RoutedEventArgs e)
        {
            Process p = getAriaProcessOrStart(ARIA_EXE_PATH);
        }

        private void OnAriaOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            txtAriaLog.AppendText(e.Data);
        }

        static IEnumerable<string> TailFrom(string file)
        {
            using (FileStream fs = File.Open(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var reader = new StreamReader(fs))
                {
                    while (true)
                    {
                        string line = reader.ReadLine();
                        if (reader.BaseStream.Length < reader.BaseStream.Position)
                            reader.BaseStream.Seek(0, SeekOrigin.Begin);

                        yield return line;
                    }
                }
            }
        }

        private RepeatCommandMacro Macro;
        private void btnSendMessage_Click(object sender, RoutedEventArgs e)
        {
            /*string source = @"System.Console.WriteLine(""Hello benohead.com !"");";

            var executed = new Evaluator(new CompilerContext(new CompilerSettings(), new ConsoleReportPrinter())).Run(source);*/

            /*var message = new Maria.IPC.Messages.TriggerAction.Request()
            {
                ActionName = txtMessage.Text
            };

            _client.SendMessage(message);*/

            if(Macro == null)
            {
                Macro = new RepeatCommandMacro(_client);
                Macro.Start();
            }
            else if(Macro != null)
            {
                Macro.Stop();
            }
            
            
        }

        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            _client.Connect();
        }

        private void btnOpenEditor_Click(object sender, RoutedEventArgs e)
        {
            var editor = new Editor();
            editor.Show();
        }
    }
}
