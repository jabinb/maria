﻿using Maria.Game;
using Maria.Game.Messages.Client;
using Maria.IPC;
using Maria.IPC.Client;
using Maria.IPC.Messages;
using Maria.Util;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MariaGUI.Macro
{
    class RepeatCommandMacro
    {
        private ServiceClient _client;

        private ConcurrentDictionary<Type, IMessage> messageTypesWaited = new ConcurrentDictionary<Type, IMessage>();
        private PlayerProps.Response LastProps = null;

        private CancellationTokenSource TokenSource;
        private CancellationToken Token;

        readonly object lockObj = new object();

        public RepeatCommandMacro(ServiceClient client)
        {
            _client = client;
            client.RegisterReceivedCallback(OnMessageReceived);
        }

        private void OnMessageReceived(IMessage message)
        {

            messageTypesWaited.TryAdd(message.GetType(), message);

            if(message is PlayerProps.Response)
            {
                lock(lockObj)
                {
                    LastProps = (PlayerProps.Response)message;
                }
            }
        }

        private async Task<T> WaitForMessage<T>(int ms = 10000, Predicate<T> predicate = null) where T : class
        {
            int delayedMs = 0;

            //Haven't used one of these in a while!
            Loop:
            while(!messageTypesWaited.ContainsKey(typeof(T)))
            {
                if (delayedMs > ms)
                {
                    return null;
                }

                delayedMs += 10;
                await Task.Delay(10);
            }

            messageTypesWaited.TryRemove(typeof(T), out IMessage value);

            T result = (T)value;

            if (predicate != null && !predicate(result))
            {
                goto Loop;
            }


            return result;
        }

        private async Task<T> WaitForGameMessage<T>(int ms = 10000, Predicate<T> predicate = null) where T : class
        {
            string MessageName = MessageProcessor.GameMessageMap.FirstOrDefault(x => x.Value is T).Key;

            GameMessage gameMessage = await WaitForMessage<GameMessage>(ms, (message) => message.MessageName == MessageName);
           // T result = gameMessage.DeserializeAs<T>();
            return null;
        }

        private async Task<string> WaitForTarget(int ms = 10000)
        {
            int delayedMs = 0;
            while(LastProps == null || LastProps.CurTargetRequestType == "None")
            {
                if (delayedMs > ms)
                {
                    return null;
                }

                delayedMs += 300;
                await Task.Delay(300);
            }

            if (LastProps != null)
            {
                lock (lockObj)
                {
                    return LastProps.CurTargetRequestType;
                }
            }

            return null;
        }

        private double? GetStat(string name)
        {
            if(LastProps != null && LastProps.StatValues.ContainsKey(name))
            {
                lock(lockObj)
                {
                    return LastProps.StatValues[name];
                }
            }

            return null;
        }

        private void TriggerAction(string action)
        {
            var message = new Maria.IPC.Messages.TriggerAction.Request()
            {
                ActionName = action
            };

            _client.SendMessage(message);
        }

        public void Start()
        {
            if(TokenSource == null || TokenSource.IsCancellationRequested)
            {
                TokenSource = new CancellationTokenSource();
                Token = TokenSource.Token;
            }

            Task.Factory.StartNew(Main, Token);
        }

        public void Stop()
        {
            if(Token != null)
            {
                TokenSource.Cancel();
            }
        }

        private async void Main()
        {
            while (true)
            {
                var Health = GetStat("Health");

                TriggerAction("HotbarItemAction|22");
                await Task.Delay(500);
                TriggerAction("TargetSelfAction");


                if (Token.IsCancellationRequested)
                {
                    // Clean up here, then...
                    return;
                }
            }
        }

    }
}
