﻿using System;
using Maria.IPC;
using UnityEngine;
using System.Reflection;
using Maria.IPC.Server;
using Maria.Util;
using Maria.Game;

namespace Maria.Inject
{
    public class EntryPoint
    {
        public static void Main()
        {
            Logging.Logger = new UnityLogger();

            Logging.Out("MariaInject Main called");

            var manager = GameObject.CreatePrimitive(PrimitiveType.Cube).AddComponent<ServiceObject>();
            manager.enabled = true;


            Logging.Out("MariaInject Main Finished");
        }
    }
}
