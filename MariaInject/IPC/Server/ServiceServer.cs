﻿using Lidgren.Network;
using Maria.IPC.Messages;
using Maria.Util;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Maria.IPC.Server
{
    public class ServiceServer : ServicePeer
    {
        public ServiceServer() : base()
        {
            // set up network
            NetPeerConfiguration config = new NetPeerConfiguration("Maria")
            {
                MaximumConnections = 100,
                Port = MARIA_PORT
            };

            NetServer _server = new NetServer(config);
            _peer = _server;
            _messageProcessor = new MessageProcessor();

        }

        public void Host()
        {
            Logging.Out("MariaInject ServiceServer::Host() begin");

            _peer.Start();

            Logging.Out("MariaInject ServiceServer::Host() Status " + _peer.Status.ToString());
            Logging.Out("MariaInject ServiceServer::Host() end");

        }

        public void ProcessMessages()
        {
            this.ReceivedNetMessage(_peer);
        }

        public void Shutdown(string message = "Shutdown requested")
        {
            Logging.Out("MariaInject ServiceServer::Shutdown()");

            _peer.Shutdown(message);
        }
    }
}
