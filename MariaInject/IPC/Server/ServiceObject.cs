﻿
using UnityEngine;
using Maria.IPC.Messages;
using Maria.Util;
using Maria.Game;

namespace Maria.IPC.Server
{
    public class ServiceObject : MonoBehaviour
    {
        private static ServiceObject _Instance;
        private ServiceServer _server;
        private MessageDispatcher _messageDispatcher;
        private MariaMessageModule _messageModule;

        private ServiceObject() { }

        public static ServiceObject Instance
        {
            get => _Instance;
        }

        private void Start()
        {
            _Instance = this;
            //Logging.Out("MariaInject ServiceManager::Start() begin");

            UnityEngine.Object.DontDestroyOnLoad(this);

            _server = new ServiceServer();
            _messageDispatcher = new MessageDispatcher();

            InvokeRepeating("SendGameUpdate", 2.0f, 0.3f);

            _server.RegisterReceivedCallback(this.OnMessageReceived);
            _server.Host();

            //Logging.Out("MariaInject ServiceManager::Start() end");

        }

        void OnMessageReceived(object state)
        {
            //Logging.Out("OnMessageReceived ", state.GetType().FullName);
            IMessage message = (IMessage)state;

            if(message is IRequest)
            {
                IResponse response = _messageDispatcher.Dispatch((IRequest)message);

                if(response != null)
                {
                    //Logging.Out("OnMessageReceived sending ", response.GetType().FullName);

                    _server.SendMessage(response);
                }
            }
        }

        void TryProcesGameMessages()
        {
            if(_messageModule == null)
            {
                var app = GameObjectSingleton<ApplicationController>.Instance;
                if (app != null && app.ClientModule != null && app.ClientModule.MessageSystem != null)
                {
                    _messageModule = new MariaMessageModule(app.ClientModule.MessageSystem, _server);
                    app.ClientModule.MessageSystem.RegisterModule(_messageModule);
                    Logging.Out("MariaMessageSystem Registered");
                }
            }
            else
            {
                _messageModule.ProcessMessages();
            }
        }

        void Update()
        {
            _server.ProcessMessages();
            TryProcesGameMessages();
        }

        void SendGameUpdate()
        {
            if(GameObjectSingleton<ApplicationController>.Instance.CurGameState == ApplicationController.GameState.Game)
            {
                _server.SendMessage(new PlayerProps.Request().Execute());
            }
        }

        void OnDestroy()
        {
            _server.Shutdown("Game closed");
        }

    }
}
