﻿using Maria.IPC.Messages;
using Maria.Util;
using System;
using UnityEngine;

namespace Maria.IPC.Server
{
    class MessageDispatcher
    {
        public IResponse Dispatch(IRequest request)
        {
            Logging.Out("Dispatching request type: " + request.GetType().FullName);

            IResponse response = null;

            try
            {
                response = request.Execute();
                response.RemoteEndPoint = request.RemoteEndPoint;
                response.Id = request.Id;
                Logging.Out("Executed returned response: " + response.GetType().FullName);

            }
            catch(Exception e)
            {
                response = new ErrorResponse()
                {
                    ExceptionMessage = e.Message,
                    ExceptionType = e.GetType().Name
                };

                Logging.Out("Dispatch caught exception {0}", e.Message);
            }

            return response;
        }
    }
}
