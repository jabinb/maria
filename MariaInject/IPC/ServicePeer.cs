﻿using Lidgren.Network;
using Maria.IPC.Messages;
using Maria.Util;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;

namespace Maria.IPC
{
    public class ServicePeer
    {
        protected const int MARIA_PORT = 14141;
        protected const string MARIA_HOST = "localhost";

        public delegate void OnReceivedMessage(IMessage message);
        private List<OnReceivedMessage> _receiveCallbacks;
        protected NetPeer _peer;
        protected MessageProcessor _messageProcessor;

        public void SendMessage(IMessage message)
        {
            Logging.Out("SendMessage with type " + message.GetType().FullName);

            if(message.RemoteEndPoint == null && _peer is NetClient)
                message.RemoteEndPoint = (_peer as NetClient).ServerConnection.RemoteEndPoint;

            if (message.RemoteEndPoint == null && _peer is NetServer)
            {
                BroadcastMessage(message);
                return;
            }

            if (message.RemoteEndPoint == null)
                throw new NetException("Message is missing a connection to send to!");

            NetOutgoingMessage om = _peer.CreateMessage();

            Logging.Out("Sending to peer " + message.RemoteEndPoint);

            try
            {
                _messageProcessor.WriteMessage(message, om);
                _peer.SendMessage(om, GetConnectionByEndpoint(message.RemoteEndPoint), NetDeliveryMethod.ReliableOrdered);
                _peer.FlushSendQueue();
            }
            catch(Exception e)
            {
                Logging.Out(e.Message);
            }

            Logging.Out("SendMessage end " + _peer.Connections.Count);

        }

        public void BroadcastMessage(IMessage message)
        {
            NetOutgoingMessage om = _peer.CreateMessage();
            Logging.Out("Sending broadcast message");

            try
            {
                _messageProcessor.WriteMessage(message, om);
                _peer.SendMessage(om, _peer.Connections, NetDeliveryMethod.ReliableOrdered, 0);
                _peer.FlushSendQueue();
            }
            catch (Exception e)
            {
                Logging.Out(e.Message);
            }

        }

        private NetConnection GetConnectionByEndpoint(IPEndPoint ep)
        {
            return _peer.GetConnection(ep);
        }

        public void RegisterReceivedCallback(OnReceivedMessage callback)
        {

            if (_receiveCallbacks == null)
                _receiveCallbacks = new List<OnReceivedMessage>();
            _receiveCallbacks.Add(callback);
        }

        public void UnregisterReceivedCallback(OnReceivedMessage callback)
        {
            if (_receiveCallbacks == null)
                return;

            // remove all callbacks regardless of sync context
            _receiveCallbacks.RemoveAll(item => item.Equals(callback));

            if (_receiveCallbacks.Count < 1)
                _receiveCallbacks = null;
        }

        protected void PostReceiveCallbacks(IMessage message)
        {
            if (_receiveCallbacks != null)
            {
                foreach (var callback in _receiveCallbacks)
                {
                    try
                    {
                        callback(message);
                    }
                    catch (Exception)
                    {

                    }
                }
            }
        }

        protected void ReceivedNetMessage(object peer)
        {
            NetIncomingMessage im;
            while ((im = this._peer.ReadMessage()) != null)
            {
                //Logging.Out("Lidgren message type received: " + im.MessageType.ToString());

                // handle incoming message
                switch (im.MessageType)
                {
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.ErrorMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.VerboseDebugMessage:
                        string text = im.ReadString();
                        Logging.Out(text);
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)im.ReadByte();

                        string reason = im.ReadString();
                        Logging.Out(NetUtility.ToHexString(im.SenderConnection.RemoteUniqueIdentifier) + " " + status + ": " + reason);

                        if (status == NetConnectionStatus.Connected && im.SenderConnection.RemoteHailMessage != null)
                            Logging.Out("Remote hail: " + im.SenderConnection.RemoteHailMessage.ReadString());
                        break;
                    case NetIncomingMessageType.Data:

                        IMessage message = _messageProcessor.ReadMessage(im);

                        if (message != null)
                        {
                            PostReceiveCallbacks(message);
                        }
                        break;
                    default:
                        Logging.Out("Unhandled type: " + im.MessageType + " " + im.LengthBytes + " bytes");
                        break;
                }
                this._peer.Recycle(im);
            }
        }
    }
}
