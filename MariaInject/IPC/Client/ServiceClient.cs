﻿using Lidgren.Network;
using Maria.IPC.Messages;
using Maria.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Maria.IPC.Client
{
    public class ServiceClient : ServicePeer
    {
        public ServiceClient() : base()
        {
            NetPeerConfiguration config = new NetPeerConfiguration("Maria");
            _peer = new NetClient(config);
            _peer.RegisterReceivedCallback(this.ReceivedNetMessage);
            _messageProcessor = new MessageProcessor();
        }

        public void Connect()
        {
            NetOutgoingMessage hail = _peer.CreateMessage("This is the hail message");
            _peer.Start();
            _peer.Connect(MARIA_HOST, MARIA_PORT, hail);

            Logging.Out("Connected to AriaService");
        }
    }
}
