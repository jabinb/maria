﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

namespace Maria.IPC.Messages
{
    public class SendScriptCommand
    {
        [ProtoContract]
        public class Request : Message, IRequest
        {
            [ProtoMember(1)]
            public string Command;
            [ProtoMember(2)]
            public ulong TargetObj = 0UL;

            public IResponse Execute()
            {

                GetClientModule().SendScriptCommand(Command, TargetObj);

                return new Response()
                {
                    Command = Command
                };
            }
        }

        [ProtoContract]
        public class Response : Message, IResponse
        {
            [ProtoMember(2)]
            public string Command;

            public Response()
            {
            }
        }
    }
}
