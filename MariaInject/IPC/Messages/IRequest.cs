﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Maria.IPC.Messages
{
    public interface IRequest : IMessage
    {
        IResponse Execute();
    }
}
