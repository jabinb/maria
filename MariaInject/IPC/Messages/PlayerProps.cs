﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

namespace Maria.IPC.Messages
{
    public class PlayerProps
    {
        [ProtoContract]
        public class Request : Message, IRequest
        {
            public IResponse Execute()
            {
                var response = new Response();

                var player = GetLocalPlayer();

                if (player != null)
                {
                    foreach (var item in player.StatValues)
                    {
                        response.StatValues.Add(item.Key, item.Value.CurValue);
                    }

                    var CurTargetRequestType = GetApplicationController().InputController.CurTargetRequestType.ToString();

                    response.CurTargetRequestType = CurTargetRequestType;
                }

                return response;
            }
        }

        [ProtoContract]
        public class Response : Message, IResponse
        {
            [ProtoMember(1)]
            public Dictionary<string, double> StatValues = new Dictionary<string, double>();

            public string CurTargetRequestType;

        }
    }
}
