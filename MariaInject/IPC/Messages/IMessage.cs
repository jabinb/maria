﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Maria.IPC.Messages
{
    public interface IMessage
    {
        Guid Id { get; set; }

        IPEndPoint RemoteEndPoint { get; set; }
    }

}
