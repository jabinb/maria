﻿using Maria.IPC.Messages;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Maria.IPC.Messages
{
    [ProtoContract]
    public class ErrorResponse : Message, IResponse
    {
        [ProtoMember(1)]
        public string ExceptionMessage;

        [ProtoMember(2)]
        public string ExceptionType;

        public ErrorResponse()
        {
        }
    }
}
