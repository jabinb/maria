﻿using Maria.Util;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

namespace Maria.IPC.Messages
{
    public class TriggerAction
    {
        [ProtoContract]
        public class Request : Message, IRequest
        {
            [ProtoMember(1)]
            public string ActionName;

            public IResponse Execute()
            {
                bool successful = false;

                try
                {

                    var x = typeof(ApplicationController).Assembly;
                    PlayerAction action;

                    if(ActionName.Contains("HotbarItemAction"))
                    {
                        var parts = ActionName.Split('|');
                        new HotbarItemAction(int.Parse(parts[1])).Execute();
                    }
                    else
                    {
                        Type ActionType = x.GetType(ActionName);
                        action = (PlayerAction)Activator.CreateInstance(ActionType);
                        action.Execute();
                    }

                    Logging.Out("TriggerAction requested {0}", ActionName);

                    successful = true;
                }
                catch(Exception e)
                {
                    Logging.Out("TriggerAction requested {0}, caught exception: {1}", ActionName, e.Message);
                }


                return new Response()
                {
                    Successful = successful
                };
            }
        }

        [ProtoContract]
        public class Response : Message, IResponse
        {
            [ProtoMember(1)]
            public bool Successful;

            public Response()
            {
            }
        }
    }
}
