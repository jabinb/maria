﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

namespace Maria.IPC.Messages
{
    public class Echo
    {
        [ProtoContract]
        public class Request : Message, IRequest
        {
            [ProtoMember(1)]
            public string message;

            public IResponse Execute()
            {
                return new Response()
                {
                    message = message
                };
            }
        }

        [ProtoContract]
        public class Response : Message, IResponse
        {
            [ProtoMember(1)]
            public string message;

            public Response()
            {
            }
        }
    }
}
