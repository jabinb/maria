﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.IPC.Messages
{
    [ProtoContract]
    public class GameMessage : Message
    {
        [ProtoMember(1)]
        public string MessageName;

        [ProtoMember(2)]
        public byte[] Data;

        public T DeserializeAs<T>() where T : class
        {
            return MessageProcessor.DeserializeData<T>(Data);
        }
    }
}
