﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.Serialization;
using Lidgren.Network;
using ProtoBuf;
using UnityEngine;

namespace Maria.IPC.Messages
{
    [ProtoContract]
    [ProtoInclude(2, typeof(Echo.Request))]
    [ProtoInclude(3, typeof(Echo.Response))]
    [ProtoInclude(4, typeof(PlayerProps.Request))]
    [ProtoInclude(5, typeof(PlayerProps.Response))]
    [ProtoInclude(6, typeof(SendScriptCommand.Request))]
    [ProtoInclude(7, typeof(SendScriptCommand.Response))]
    [ProtoInclude(8, typeof(TriggerAction.Request))]
    [ProtoInclude(9, typeof(TriggerAction.Response))]
    [ProtoInclude(10, typeof(Eval.Request))]
    [ProtoInclude(11, typeof(Eval.Response))]
    [ProtoInclude(12, typeof(GameMessage))]
    public abstract class Message : IMessage
    {
        private Guid _Id = Guid.NewGuid();

        [ProtoMember(1)]
        public Guid Id { get => _Id; set => _Id = value; }

        private IPEndPoint _Connection;

        public IPEndPoint RemoteEndPoint { get => _Connection; set => _Connection = value; }

        public ClientModule GetClientModule()
        {
            return GetApplicationController().ClientModule;
        }

        public ApplicationController GetApplicationController()
        {
            return GameObjectSingleton<ApplicationController>.Instance;
        }

        public LocalPlayer GetLocalPlayer()
        {
            return GetApplicationController().Player;
        }

        public GameUI GetGameUI()
        {
            return GetApplicationController().GameUI;
        }
    }
}
