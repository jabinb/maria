﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;
using Mono.CSharp;
using ProtoBuf;

namespace Maria.IPC.Messages
{
    public class Eval
    {
        [ProtoContract]
        public class Request : Message, IRequest
        {
            public string source;

            public IResponse Execute()
            {

                string source = @"UnityEngine.Debug.LogError(""Hello benohead.com !"");";

                var executed = new Evaluator(new CompilerContext(new CompilerSettings(), new ConsoleReportPrinter())).Run(source);
                
                return new Response()
                {
                    source = source,
                    //executed = executed
                };
            }
        }

        [ProtoContract]
        public class Response : Message, IResponse
        {
            public string source;
            public bool executed;

            public Response()
            {
            }
        }
    }
}
