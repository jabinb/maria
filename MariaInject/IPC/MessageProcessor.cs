﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Maria.IPC.Messages;
using System.IO;
using Maria.Util;
using ProtoBuf;
using Maria.Game.Messages.Client;
using Maria.Game.Messages.Login;

namespace Maria.IPC
{
    public class MessageProcessor
    {
        public static Dictionary<string, Type> messageTypes = new Dictionary<string, Type>
        {
            { typeof(Echo.Request).FullName, typeof(Echo.Request) },
            { typeof(Echo.Response).FullName, typeof(Echo.Response) },

            { typeof(Eval.Request).FullName, typeof(Eval.Request) },
            { typeof(Eval.Response).FullName, typeof(Eval.Response) },

            { typeof(PlayerProps.Request).FullName, typeof(PlayerProps.Request) },
            { typeof(PlayerProps.Response).FullName, typeof(PlayerProps.Response) },

            { typeof(TriggerAction.Request).FullName, typeof(TriggerAction.Request) },
            { typeof(TriggerAction.Response).FullName, typeof(TriggerAction.Response) },

            { typeof(SendScriptCommand.Request).FullName, typeof(SendScriptCommand.Request) },
            { typeof(SendScriptCommand.Response).FullName, typeof(SendScriptCommand.Response) },

            { typeof(GameMessage).FullName, typeof(GameMessage) },
            { typeof(ErrorResponse).FullName, typeof(ErrorResponse) },

        };

        public static Dictionary<string, Type> GameMessageMap = new Dictionary<string, Type>
        {
            { LoginFailedMessage.MessageName, typeof(LoginFailedMessage) },
            { CharacterListMessage.MessageName, typeof(CharacterListMessage) },
            { TransferRegionMessage.MessageName, typeof(TransferRegionMessage) },
            { IncomingChatMessage.MessageName, typeof(IncomingChatMessage) },
            { SystemMessage.MessageName, typeof(SystemMessage) },
            { IncomingScriptClientMessage.MessageName, typeof(IncomingScriptClientMessage) },
            { OpenContainerMessage.MessageName, typeof(OpenContainerMessage) },
            { CloseContainerMessage.MessageName, typeof(CloseContainerMessage) },
            { RequestClientTargetMessage.MessageName, typeof(RequestClientTargetMessage) },
        };

        public static Dictionary<string, Type> UnknownGameMessageMap = new Dictionary<string, Type>
        {
            { WorldStateMessage.MessageName, typeof(WorldStateMessage) },
            { ObjectUpdateMessage.MessageName, typeof(ObjectUpdateMessage) },
            { ObjectEventMessage.MessageName, typeof(ObjectEventMessage) },
            { PermanentObjectStateUpdateMessage.MessageName, typeof(PermanentObjectStateUpdateMessage) },
            { OpenDynamicWindowMessage.MessageName, typeof(OpenDynamicWindowMessage) },
            { CloseDynamicWindowMessage.MessageName, typeof(CloseDynamicWindowMessage) },
            { OpenHtmlWindowMessage.MessageName, typeof(OpenHtmlWindowMessage) },
            { CloseHtmlWindowMessage.MessageName, typeof(CloseHtmlWindowMessage) },
            { ExecuteJavascriptMessage.MessageName, typeof(ExecuteJavascriptMessage) },
            { ContextMenuItemsMessage.MessageName, typeof(ContextMenuItemsMessage) },
            { OpenCustomContextMenuMessage.MessageName, typeof(OpenCustomContextMenuMessage) },
            { ChangeAttachedObjectMessage.MessageName, typeof(ChangeAttachedObjectMessage) },
            { TransferInitiatedMessage.MessageName, typeof(TransferInitiatedMessage) },
            { ServerListUpdateMessage.MessageName, typeof(ServerListUpdateMessage) }

        };

        public IMessage ReadMessage(NetIncomingMessage incoming)
        {

            incoming.ReadString(out string messageType);

            //Logging.Out("Reading message: " + messageType);

            if (!messageTypes.ContainsKey(messageType))
            {
                Logging.Out("[Maria] Bad message Type: " + messageType);
                return null;
            }

            Type t = messageTypes[messageType];

            incoming.ReadInt32(out int length);
            incoming.ReadBytes(length, out byte[] data);

            IMessage message = (IMessage)DeserializeData(t, data);
            message.RemoteEndPoint = incoming.SenderConnection.RemoteEndPoint;

            //Logging.Out("ReadMessage deserialized type: " + t.FullName);

            return message;
        }


        public NetOutgoingMessage WriteMessage(IMessage message, NetOutgoingMessage outgoing)
        {
            outgoing.Write(message.GetType().FullName);

            //Logging.Out("Type written: " + message.GetType().FullName);

            byte[] data = SerializeMessage(message);

            outgoing.Write(data.Length);
            outgoing.Write(data);

            //Logging.Out("WriteMessage serialized");

            return outgoing;

        }

        public NetOutgoingMessage Process(NetIncomingMessage incoming, NetOutgoingMessage outgoing)
        {
            var message = ReadMessage(incoming);

            if(message is IRequest)
            {
                var response = (message as IRequest).Execute();
                //Set the Id of this response to the originating request so the other party knows how to marshal this response
                response.Id = message.Id;


                //Logging.Out("Request executed, response type " + response.GetType().FullName);
                outgoing = WriteMessage(response, outgoing);
                return outgoing;
            }
            else if(message is IResponse)
            {
                Console.WriteLine("Response received but unhandled");
            }

            return null;

        }

        public static string XmlSerializeToString(object objectInstance)
        {
            var serializer = new XmlSerializer(objectInstance.GetType());
            var sb = new StringBuilder();

            using (TextWriter writer = new StringWriter(sb))
            {
                serializer.Serialize(writer, objectInstance);
            }

            return sb.ToString();
        }

        public static T DeserializeFromXmlString<T>(string xmlString)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (TextReader reader = new StringReader(xmlString))
            {
                return (T)serializer.Deserialize(reader);
            }
        }

        public static byte[] SerializeMessage(object _message)
        {
            byte[] buffer;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                Serializer.Serialize<object>(memoryStream, _message);
                buffer = memoryStream.GetBuffer();
            }
            return buffer;
        }

        // Token: 0x0600006D RID: 109 RVA: 0x00003B24 File Offset: 0x00001D24
        public static object DeserializeData(Type sourceType, byte[] _data)
        {
            object result;
            using (MemoryStream memoryStream = new MemoryStream(_data))
            {
                result = Serializer.NonGeneric.Deserialize(sourceType, memoryStream);
            }
            return result;
        }

        public static T DeserializeData<T>(byte[] _data) where T : class
        {
            T result;
            using (MemoryStream memoryStream = new MemoryStream(_data))
            {
                result = Serializer.Deserialize<T>(memoryStream);
            }
            return result;
        }
    }
}
