﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game.Math
{
    // Token: 0x02000074 RID: 116
    [ProtoContract]
    public struct Vec3
    {
        public float X;

        public float Y;

        public float Z;
    }
}
