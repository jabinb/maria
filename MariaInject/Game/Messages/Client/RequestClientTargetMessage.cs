﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game.Messages.Client
{
    public class RequestClientTargetMessage
    {
        public static string MessageName = "Client.RequestClientTarget";

        [ProtoMember(1)]
        public TargetRequestType TargetType;

	    [ProtoMember(2)]
        public uint PreviewId;

	    [ProtoMember(3)]
        public string PreviewCustomAssetBundle;

	    [ProtoMember(4)]
        public float X;

	    [ProtoMember(5)]
        public float Y;

	    [ProtoMember(6)]
        public float Z;

        public enum TargetRequestType
        {
            AnyObject,
            DynObject,
            Loc
        }
    }
}
