﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game.Messages.Client
{
    // Token: 0x02000579 RID: 1401
    [ProtoContract]
    public class IncomingScriptClientMessage
    {
        // Token: 0x1700066C RID: 1644
        // (get) Token: 0x06002349 RID: 9033 RVA: 0x000309F3 File Offset: 0x0002EBF3
        public static string MessageName
        {
            get
            {
                return "Client.IncomingScriptClientMessage";
            }
        }

        // Token: 0x1700066D RID: 1645
        // (get) Token: 0x0600234A RID: 9034 RVA: 0x000309FA File Offset: 0x0002EBFA
        // (set) Token: 0x0600234B RID: 9035 RVA: 0x00030A02 File Offset: 0x0002EC02
        [ProtoMember(1)]
        public ScriptClientMessage[] ClientMessages { get; set; }
	}
}
