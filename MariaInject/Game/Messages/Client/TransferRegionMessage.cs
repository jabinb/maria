﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game.Messages.Client
{
    // Token: 0x020005B6 RID: 1462
    [ProtoContract]
    public class TransferRegionMessage
    {
        // Token: 0x17000688 RID: 1672
        // (get) Token: 0x060023A5 RID: 9125 RVA: 0x00030B00 File Offset: 0x0002ED00
        public static string MessageName
        {
            get
            {
                return "Client.TransferRegionMessage";
            }
        }

        // Token: 0x0400214C RID: 8524
        [ProtoMember(1)]
        public string ServerAddress;

        // Token: 0x0400214D RID: 8525
        [ProtoMember(2)]
        public string RegionAddress;

        // Token: 0x0400214E RID: 8526
        [ProtoMember(3)]
        public string WorldName;
    }
}
