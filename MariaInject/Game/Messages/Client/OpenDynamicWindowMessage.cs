﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game.Messages.Client
{
    [ProtoContract]
    class OpenDynamicWindowMessage
    {
        public static string MessageName = "Client.OpenDynamicWindow";

        // Token: 0x040020B6 RID: 8374
        [ProtoMember(1)]
        public ulong TargetId;

        // Token: 0x040020B7 RID: 8375
        [ProtoMember(2)]
        public string WindowId;

	    // Token: 0x040020B8 RID: 8376
	    [ProtoMember(3)]
        public WindowFormatData WindowFormat;
    }


    [ProtoContract]
    internal class WindowFormatData
    {
        // Token: 0x040020AD RID: 8365
        [ProtoMember(1)]
        public string _unknown;

	    // Token: 0x040020AE RID: 8366
	    [ProtoMember(2)]
        public string Title;

        // Token: 0x040020AF RID: 8367
        [ProtoMember(3)]
        public double Width;

        // Token: 0x040020B0 RID: 8368
        [ProtoMember(4)]
        public double Height;

        // Token: 0x040020B1 RID: 8369
        [ProtoMember(5)]
        public double X;

        // Token: 0x040020B2 RID: 8370
        [ProtoMember(6)]
        public double Y;

        // Token: 0x040020B3 RID: 8371
        [ProtoMember(7)]
        public string PrefabName;

        // Token: 0x040020B4 RID: 8372
        [ProtoMember(8)]
        public string Anchor;

        // Token: 0x040020B5 RID: 8373
        //[ProtoMember(9)]
        //public 㐨㠯썱攴뇠\uFFFD邋鲦[] 銂烱灑짱ᗚ\u2F5F튩\uF238;
    }
}
