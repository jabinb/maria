﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game.Messages.Client
{
    // Token: 0x02000578 RID: 1400
    [ProtoContract]
    public class ScriptClientMessage
    {
        // Token: 0x1700066B RID: 1643
        // (get) Token: 0x06002346 RID: 9030 RVA: 0x000309E2 File Offset: 0x0002EBE2
        // (set) Token: 0x06002347 RID: 9031 RVA: 0x000309EA File Offset: 0x0002EBEA
        [ProtoMember(2)]
        public ClientMessageData MessageData { get; set; }

        // Token: 0x040020B9 RID: 8377
        [ProtoMember(1)]
        public string ClientMessageName;
	}

    // Token: 0x0200057B RID: 1403
    [ProtoContract]
    public sealed class ClientMessageData<T> : ClientMessageData
    {
        // Token: 0x1700066F RID: 1647
        // (get) Token: 0x06002352 RID: 9042 RVA: 0x00030A13 File Offset: 0x0002EC13
        // (set) Token: 0x06002353 RID: 9043 RVA: 0x00030A1B File Offset: 0x0002EC1B
        [ProtoMember(1)]
        public T Value { get; set; }

        // Token: 0x17000670 RID: 1648
        // (get) Token: 0x06002354 RID: 9044 RVA: 0x00030A24 File Offset: 0x0002EC24
        // (set) Token: 0x06002355 RID: 9045 RVA: 0x00030A31 File Offset: 0x0002EC31
        public override object UntypedValue
        {
            get
            {
                return this.Value;
            }
            set
            {
                this.Value = (T)((object)value);
            }
        }
    }

    // Token: 0x0200057A RID: 1402
    [ProtoInclude(7, typeof(ClientMessageData<bool>))]
    [ProtoContract]
    [ProtoInclude(6, typeof(ClientMessageData<string>))]
    [ProtoInclude(3, typeof(ClientMessageData<ulong>))]
    [ProtoInclude(5, typeof(ClientMessageData<float>))]
    [ProtoInclude(11, typeof(ClientMessageData<Dictionary<ClientMessageData, ClientMessageData>>))]
    //[ProtoInclude(12, typeof(ClientMessageData<Vec3>))]
    [ProtoInclude(9, typeof(ClientMessageData<List<ClientMessageData>>))]
    [ProtoInclude(8, typeof(ClientMessageData<double>))]
    [ProtoInclude(4, typeof(ClientMessageData<int>))]
    [ProtoInclude(10, typeof(ClientMessageData<ClientMessageData[]>))]
    public abstract class ClientMessageData
    {
        // Token: 0x1700066E RID: 1646
        // (get) Token: 0x0600234D RID: 9037
        // (set) Token: 0x0600234E RID: 9038
        public abstract object UntypedValue { get; set; }

        // Token: 0x0600234F RID: 9039 RVA: 0x000E8FC0 File Offset: 0x000E71C0
        public static ClientMessageData<T> Create<T>(T value)
        {
            return new ClientMessageData<T>
            {
                Value = value
            };
        }

        // Token: 0x06002350 RID: 9040 RVA: 0x000E8FDC File Offset: 0x000E71DC
        public static ClientMessageData CreateDynamic(object value)
        {
            Type type = value.GetType();
            TypeCode typeCode = Type.GetTypeCode(value.GetType());
            switch (typeCode)
            {
                case TypeCode.Int32:
                    return ClientMessageData.Create<int>((int)value);
                default:
                    if (typeCode != TypeCode.Boolean)
                    {
                        ClientMessageData clientMessageData = (ClientMessageData)Activator.CreateInstance(typeof(ClientMessageData<>).MakeGenericType(new Type[]
                        {
                        type
                        }));
                        clientMessageData.UntypedValue = value;
                        return clientMessageData;
                    }
                    return ClientMessageData.Create<bool>((bool)value);
                case TypeCode.UInt64:
                    return ClientMessageData.Create<ulong>((ulong)value);
                case TypeCode.Single:
                    return ClientMessageData.Create<float>((float)value);
                case TypeCode.Double:
                    return ClientMessageData.Create<double>((double)value);
                case TypeCode.String:
                    return ClientMessageData.Create<string>((string)value);
            }
        }
    }
}
