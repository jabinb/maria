﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game.Messages.Client
{
    [ProtoContract]
    class IncomingChatMessage
    {
        // Token: 0x17000676 RID: 1654
        // (get) Token: 0x0600237C RID: 9084 RVA: 0x00030A82 File Offset: 0x0002EC82
        public static string MessageName
        {
		    get
		    {
			    return "Client.IncomingChatMessage";
		    }
        }

        // Token: 0x04002114 RID: 8468
        [ProtoMember(1)]
        public ChatMessageData[] ChatMessages;
    }

    // Token: 0x0200059D RID: 1437
    [ProtoContract]
    public class ChatMessageData
    {
	    // Token: 0x04002110 RID: 8464
	    [ProtoMember(1)]
        public ulong objectRef;

        // Token: 0x04002111 RID: 8465
        [ProtoMember(2)]
        public string CharName;

	    // Token: 0x04002112 RID: 8466
	    [ProtoMember(3)]
        public string Text;

        // Token: 0x04002113 RID: 8467
        [ProtoMember(4)]
        public string MessageType;
    }

}
