﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game.Messages.Client
{
    [ProtoContract]
    class CloseContainerMessage
    {
        public static string MessageName = "Client.CloseContainer";

        [ProtoMember(1)]
        public ulong ContainerId;
    }
}
