﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game.Messages.Client
{

    // Token: 0x0200059F RID: 1439
    [ProtoContract]
    public class SystemMessage
    {
        // Token: 0x17000677 RID: 1655
        // (get) Token: 0x0600237E RID: 9086 RVA: 0x00030A89 File Offset: 0x0002EC89
        public static string MessageName
        {
            get
            {
                return "Client.SystemMessage";
            }
        }

        // Token: 0x04002115 RID: 8469
        [ProtoMember(1)]
        public string Message;

        // Token: 0x04002116 RID: 8470
        [ProtoMember(2)]
        public string MessageType;
    }
}
