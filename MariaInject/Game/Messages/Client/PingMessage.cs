﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game.Messages.Client
{
    // Token: 0x020005AF RID: 1455
    [ProtoContract]
    public class PingMessage
    {
        // Token: 0x17000685 RID: 1669
        // (get) Token: 0x0600239B RID: 9115 RVA: 0x00030AEB File Offset: 0x0002ECEB
        public static string MessageName
        {
            get
            {
                return "Client.PingMessage";
            }
        }
    }
}
