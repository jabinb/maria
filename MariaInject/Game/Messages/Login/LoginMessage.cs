﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game.Messages.Login
{
    // Token: 0x020005AC RID: 1452
    [ProtoContract]
    public class LoginMessage
    {
        // Token: 0x17000682 RID: 1666
        // (get) Token: 0x06002395 RID: 9109 RVA: 0x00030AD6 File Offset: 0x0002ECD6
        public static string MessageName
        {
            get
            {
                return "SharedClient.LoginMessage";
            }
        }

        // Token: 0x04002138 RID: 8504
        [ProtoMember(1)]
        public string UserId;

        // Token: 0x04002139 RID: 8505
        [ProtoMember(2)]
        public string ClientVersion;
    }
}
