﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game.Messages.Login
{
    // Token: 0x020005AD RID: 1453
    [ProtoContract]
    public class CreateCharacterMessage
    {
        // Token: 0x17000683 RID: 1667
        // (get) Token: 0x06002397 RID: 9111 RVA: 0x00030ADD File Offset: 0x0002ECDD
        public static string MessageName
        {
            get
            {
                return "SharedClient.CreateCharacter";
            }
        }

        // Token: 0x0400213A RID: 8506
        [ProtoMember(1)]
        public string CharacterName;

        // Token: 0x0400213B RID: 8507
        [ProtoMember(2)]
        public bool IsImmortal;

        // Token: 0x0400213C RID: 8508
        [ProtoMember(3)]
        public string StartingTemplate;
    }
}
