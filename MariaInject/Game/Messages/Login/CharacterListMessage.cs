﻿using Maria.Game.Math;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game.Messages.Login
{
    // Token: 0x020005B4 RID: 1460
    [ProtoContract]
    public class CharacterListMessage
    {
        // Token: 0x17000686 RID: 1670
        // (get) Token: 0x060023A1 RID: 9121 RVA: 0x00030AF2 File Offset: 0x0002ECF2
        public static string MessageName
        {
            get
            {
                return "Client.CharacterList";
            }
        }

        // Token: 0x04002148 RID: 8520
        [ProtoMember(1)]
        public CharacterListEntry[] Characters;

        // Token: 0x04002149 RID: 8521
        [ProtoMember(2)]
        public string AvailableImmortalType;

        // Token: 0x0400214A RID: 8522
        [ProtoMember(3)]
        public PlayerTemplateEntry[] StartingTemplates;
    }

    // Token: 0x020005B2 RID: 1458
    [ProtoContract]
    public class CharacterListEntry
    {
        // Token: 0x04002143 RID: 8515
        [ProtoMember(1)]
        public string CharacterName;

        // Token: 0x04002144 RID: 8516
        [ProtoMember(2)]
        public string ImmortalType;

        // Token: 0x04002145 RID: 8517
        [ProtoMember(3)]
        public CharListHeaderData HeaderData;
    }

    // Token: 0x020005B3 RID: 1459
    [ProtoContract]
    public class PlayerTemplateEntry
    {
        // Token: 0x04002146 RID: 8518
        [ProtoMember(1)]
        public string TemplateId;

        // Token: 0x04002147 RID: 8519
        [ProtoMember(2)]
        public string DisplayName;
    }

    // Token: 0x020005B1 RID: 1457
    [ProtoContract]
    public class CharListHeaderData
    {
        // Token: 0x04002142 RID: 8514
        [ProtoMember(1)]
        public CharListObjectData[] HeaderObjects;
    }

    // Token: 0x020005B0 RID: 1456
    [ProtoContract]
    public class CharListObjectData
    {
        // Token: 0x0400213E RID: 8510
        [ProtoMember(1)]
        public uint ClientId;

        // Token: 0x0400213F RID: 8511
        [ProtoMember(2)]
        public uint Hue;

        // Token: 0x04002140 RID: 8512
        [ProtoMember(3)]
        public Vec3 Scale;

        // Token: 0x04002141 RID: 8513
        [ProtoMember(4)]
        public string Variation;
    }

}
