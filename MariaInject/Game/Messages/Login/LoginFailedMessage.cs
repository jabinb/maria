﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game.Messages.Login
{
    // Token: 0x020005B5 RID: 1461
    [ProtoContract]
    public class LoginFailedMessage
    {
        // Token: 0x17000687 RID: 1671
        // (get) Token: 0x060023A3 RID: 9123 RVA: 0x00030AF9 File Offset: 0x0002ECF9
        public static string MessageName
        {
            get
            {
                return "Client.RegionLoginFailed";
            }
        }

        // Token: 0x0400214B RID: 8523
        [ProtoMember(1)]
        public string Reason;
    }
}
