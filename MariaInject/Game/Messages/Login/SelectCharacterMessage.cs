﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game.Messages.Login
{
    // Token: 0x020005AE RID: 1454
    [ProtoContract]
    public class SelectCharacterMessage
    {
        // Token: 0x17000684 RID: 1668
        // (get) Token: 0x06002399 RID: 9113 RVA: 0x00030AE4 File Offset: 0x0002ECE4
        public static string MessageName
        {
            get
            {
                return "SharedClient.SelectCharacter";
            }
        }

        // Token: 0x0400213D RID: 8509
        [ProtoMember(1)]
        public int CharacterIndex;
    }
}
