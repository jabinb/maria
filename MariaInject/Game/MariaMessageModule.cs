﻿using Maria.Game.Messages;
using Maria.Game.Messages.Client;
using Maria.Game.Messages.Login;
using Maria.IPC;
using Maria.IPC.Messages;
using Maria.IPC.Server;
using Maria.Util;
using MessageCore;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Game
{
    public class MariaMessageModule : MessageModule
    {
        private ServiceServer _server;

        public MariaMessageModule(MessageSystem _messageSystem, ServiceServer server) : base(_messageSystem, "maria", ClientModule.messageNamespaces)
        {
            _server = server;

            foreach (var item in Maria.IPC.MessageProcessor.GameMessageMap)
            {
                base.RegisterHandler(item.Key, new Action<IncomingMessage>(this.ForwardMessage));
            }

            /* Unknown messages */
            foreach (var item in Maria.IPC.MessageProcessor.UnknownGameMessageMap)
            {
                base.RegisterHandler(item.Key, new Action<IncomingMessage>(this.HandleUnknownMessage));
            }
        }


        public void HandleUnknownMessage(IncomingMessage incoming)
        {
            Logging.Out("Received unknown message {0}", incoming.Message.MessageName);
        }

        public void ForwardMessage(IncomingMessage incoming)
        {
            Logging.Out("MariaMessageModule received game message {0}", incoming.Message.MessageName);

            var forwardMessage = new GameMessage()
            {
                MessageName = incoming.Message.MessageName,
                Data = incoming.Message.Data
            };
            _server.SendMessage(forwardMessage);
        }

        public object GetProperty<T>(object instance, string propertyName)
        {
            PropertyInfo property = instance.GetType().GetProperty(propertyName);
            return (T)property.GetValue(instance, null);
        }
    }
}
