﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Maria.Util
{
    public interface ILogger
    {
        void Out(string format, params object[] arg);
        void Out(string message);
    }

    public class UnityLogger : ILogger
    {
        public void Out(string format, params object[] arg)
        {
            UnityEngine.Debug.LogFormat(format, arg);
        }

        public void Out(string message)
        {
            UnityEngine.Debug.Log(message);
        }
    }

    public class ConsoleLogger : ILogger
    {
        public void Out(string format, params object[] arg)
        {
            Console.WriteLine(format, arg);
        }

        public void Out(string message)
        {
            Console.WriteLine(message);
        }
    }

    public class Logging
    {
        private static ILogger _logger = new ConsoleLogger();

        public static ILogger Logger { get => _logger; set => _logger = value; }

        public static void Out(string format, params object[] arg)
        {
            Logger.Out(format, arg);
        }

        public static void Out(string message)
        {
            Logger.Out(message);
        }
    }
}

